import {popular, search, topRated, upcoming} from "../mildlewares/fetch.emun";
import {load, loadSearch} from "../mildlewares/fetch.middleware";
import {mapFilms} from "../mildlewares/map.middleware";
import { Response } from "../interfaces/response";

const state: { loadedFilms: string, page: number } = {
    loadedFilms: popular,
    page: 1
}

const clearContainer = (): void => {
    const mainContainer = document.getElementById("film-container");

    while (mainContainer?.firstChild) {
        mainContainer.removeChild(mainContainer.firstChild);
    }
}

// const loadFavouriteFilms = (): void => {
//     const liked: number[] = JSON.parse(localStorage.getItem("id") || "[]")
//     if (liked !== []) {
//         const films: Results = [];
//         liked.forEach(likedFilmId => films.push(loadById(likedFilmId)))
//
//     }
// }

const loadPopularFilms = (): void => {
    state.loadedFilms = popular
    state.page = 1

    load(popular, state.page).then(r => mapFilms(<Response>r))
}

const loadUpComingFilms = (): void => {
    state.loadedFilms = upcoming
    state.page = 1

    load(upcoming, state.page).then(r => mapFilms(<Response>r))
}

const loadTopRatedFilms = (): void => {
    state.loadedFilms = topRated
    state.page = 1

    load(topRated, state.page).then(r => mapFilms(<Response>r))
}

const loadSearchingFilms = (query: string): void => {
    state.loadedFilms = `${search}&query=${query}`
    state.page = 1
    loadSearch(`${search}?query=${query}`, state.page).then(r => mapFilms(<Response>r))
}

const loadMoreFilms = (): void => {
    state.page += 1
    if (state.loadedFilms.includes("query")) {
        loadSearch(state.loadedFilms, state.page).then(r => mapFilms(<Response>r))
    }
    load(state.loadedFilms, state.page).then(r => mapFilms(<Response>r))
}

const addToFavourite = (id: number): void => {
    const likes: number[] = JSON.parse(localStorage.getItem("id") || "[]")
    if (likes.includes(id)) {
        return localStorage.setItem("id", JSON.stringify(likes.filter((item: number) => item !== id)))
    }
    localStorage.setItem("id", JSON.stringify([...likes, id]))
}

export {
    loadUpComingFilms,
    loadTopRatedFilms,
    loadPopularFilms,
    loadSearchingFilms,
    loadMoreFilms,
    addToFavourite,
    clearContainer,
    // loadFavouriteFilms
}
