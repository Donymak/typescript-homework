import {
    loadMoreFilms,
    loadPopularFilms,
    loadSearchingFilms,
    loadTopRatedFilms,
    loadUpComingFilms,
    clearContainer,
    // loadFavouriteFilms
} from "../actions/actions";


const handleEvents = (): void => {

    const popular = document.getElementById("popular");
    const upcoming = document.getElementById("upcoming");
    const topRated = document.getElementById("top_rated");
    const submit = document.getElementById("submit");
    const search = document.getElementById("search");
    const loadMore = document.getElementById("load-more");

console.log(localStorage.getItem("id"))

    window.addEventListener("load",() => {
        loadPopularFilms();
        // loadFavouriteFilms()
    } );
    (<HTMLButtonElement>submit).onclick = () => {
        clearContainer();
        loadSearchingFilms(((<HTMLInputElement>search).value));
    }
    (<HTMLElement>popular).addEventListener("click", () => {clearContainer(); loadPopularFilms()});
    (<HTMLElement>upcoming).addEventListener("click", () => {clearContainer(); loadUpComingFilms()});
    (<HTMLElement>topRated).addEventListener("click", () => {clearContainer(); loadTopRatedFilms()});
    (<HTMLButtonElement>loadMore).addEventListener("click", loadMoreFilms)
}

export {handleEvents}
