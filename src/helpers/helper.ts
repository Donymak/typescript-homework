const isFavourite = (id: number): boolean => {
    const likes: number[] = JSON.parse(localStorage.getItem("id") || "[]")
    return !!likes.includes(id);
}

export {isFavourite}