
export interface Movie {
    id: number,
    title: string,
    release_date: string,
    picture: string,
    overview: string
}