
export interface Response {
    page?: number,
    results: {
        id: number,
        title: string,
        release_date: string,
        picture: string,
        poster_path: string,
        adult: boolean,
        genre_ids: number[],
        original_language: string,
        original_title: string,
        overview: string,
        popularity: number,
        video: boolean,
        vote_average: number,
        vote_count: number
    }[]
}