const apiPath = "https://api.themoviedb.org/3";
const apiKey = "api_key=797b8bd14e15fb797a4431579a878d3c";
const movie = "/movie/"
const popular = "/movie/popular";
const topRated = "/movie/top_rated";
const upcoming = "/movie/upcoming";
const search = "/search/movie"


export {apiPath, apiKey, popular, topRated, upcoming, search, movie}