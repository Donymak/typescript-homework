import {apiKey, apiPath} from "./fetch.emun";

function load<T>(req: string, page: number): Promise<T> {
    return fetch(`${apiPath}${req}?${apiKey}&page=${page}`)
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText)
            }
            return response.json()
        })

}

// function loadById<T>(id: number): Promise<T> {
//     return fetch(`${apiPath}${movie}${id}?${apiKey}`)
//         .then(response => {
//             if (!response.ok) {
//                 throw new Error(response.statusText)
//             }
//             return response.json()
//         })
//
// }

function loadSearch<T>(req: string, page: number): Promise<T> {
    return fetch(`${apiPath}${req}&${apiKey}&page=${page}`)
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText)
            }
            return response.json()
        })

}

export {
    load,
    loadSearch,
    // loadById
}