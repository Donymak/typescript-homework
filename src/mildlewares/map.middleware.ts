import {preRender} from "./render.middleware";
import {Movie} from "../interfaces/movie";
import {Response} from "../interfaces/response"



const mapFilms = (response: Response): void => {
    const fullFilms = response.results
    const clearFilms: Movie[] = []
    fullFilms.forEach((film) =>
        clearFilms.push({
            id: film.id,
            title: film.title,
            release_date: film.release_date,
            picture: `https://image.tmdb.org/t/p/w500${film.poster_path}`,
            overview: film.overview
        }))
    preRender(clearFilms)
}

export {mapFilms}