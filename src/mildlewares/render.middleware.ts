import {addToFavourite} from "../actions/actions";
import {Movie} from "../interfaces/movie";
import {isFavourite} from "../helpers/helper";


function appendChildToContainer
(container: HTMLDivElement, child: Node): void {
    container.append(child)
}

const preRender = (data: Movie[]): void => {

    const mainContainer = document.getElementById("film-container");

    data.forEach(data => {
        const filmContainer = document.createElement("div");
        const filmCard = document.createElement("div")
        const picture = document.createElement("img")
        const description = document.createElement("div")
        const paragraph = document.createElement("p")
        const date = document.createElement("div")
        const dateContent = document.createElement("small")

        const iconSvg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        const iconPath = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "path"
        );

        isFavourite(data.id) ? iconSvg.setAttribute("fill", "red") : iconSvg.setAttribute("fill", "#ff000078")

        iconSvg.setAttribute("viewBox", "0 -2 18 22");
        iconSvg.setAttribute("width", "50");
        iconSvg.setAttribute("height", "50");
        iconSvg.setAttribute("stroke", "red");
        iconSvg.setAttribute("id", `${data.id}`);
        iconSvg.setAttribute("class", "bi bi-heart-fill position-absolute p-2")
        iconSvg.onclick = () => addToFavourite(data.id)
        iconPath.setAttribute(
            "d",
            "M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
        );
        iconPath.setAttribute("fill-rule", "evenodd");

        filmContainer.className = "col-lg-3 col-md-4 col-12 p-2"
        filmCard.className = "card shadow-sm"
        description.className = "card-body"
        paragraph.className = "card-text truncate"
        date.className = "d-flex justify-content-between align-items-center"
        dateContent.className = "text-muted"

        picture.src = data.picture
        paragraph.innerText = data.overview
        dateContent.innerText = data.release_date


        iconSvg.appendChild(iconPath);
        date.appendChild(dateContent)

        description.appendChild(paragraph)
        description.appendChild(date)

        filmCard.appendChild(picture)
        filmCard.appendChild(iconSvg)
        filmCard.appendChild(description)

        filmContainer.appendChild(filmCard)

        appendChildToContainer(<HTMLDivElement>mainContainer, filmContainer)
    })

}

export {preRender}
